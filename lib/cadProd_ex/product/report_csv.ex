defmodule CadProdEx.Product.ReportCsv do
  alias CadProdEx.{Repo, SimpleQueue}

  def run do
    products = Repo.all(CadProdEx.Product)
    file_name = "products_report.csv"
    user_email = "gabriel.zuqueto@skyhub.com.br"
    path = Path.absname("./reports/products_report.csv")
    file = File.open!(path, [:write, :utf8])

    for product <- products do
      IO.write(
        file,
        "#{product.sku};#{product.name};#{product.preco};#{product.qty};#{product.ean}\n"
        )
    end
    send_report(path, file_name, user_email)
  end

  def enqueue_task, do: SimpleQueue.enqueue(__MODULE__)

  def send_report(path, file_name, user_email) do
    with {:ok, file_readed} <- File.read(path), 
         {:ok, %{status_code: 200}} <- HTTPoison.post(
          "http://localhost:4002/api/report", 
          {:multipart, [
          {"to", user_email},
          {"subject", "Relatorio de Produtos"},
          {"attachment", IO.iodata_to_binary(file_readed),
          {"form-data", [
            {"name", "\"attachment\""},
            {"filename", file_name}]},
            [{"Content-Type", "application/json"}]}]},
            []) do
    {:ok, "Email Enfileirado para envio"}
   else
     _ ->
      {:error, "Error"}      
    end
  end

end