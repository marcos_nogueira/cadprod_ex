defmodule CadProdEx.SimpleQueue do
  use GenServer

  ### GenServer API

  @doc """
  GenServer.init/1 callback
  """
  def init(state), do: {:ok, state}

  @doc """
  GenServer.handle_cast/2 callback
  """
  def handle_cast({:enqueue, module}, state) do
    result = module.run()
    {:noreply, state ++ [result]}
  end

  ### Client API / Helper functions

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def enqueue(module), do: GenServer.cast(__MODULE__, {:enqueue, module})
end