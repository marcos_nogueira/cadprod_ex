defmodule CadProdEx.Product.ReportCsvTest do
  use ExUnit.Case

  import :meck

  alias CadProdEx.Product.ReportCsv

  setup do
    new(:hackney)
    on_exit(fn -> unload() end)

    :ok
  end

  describe "Generete Reports" do
    test "post to send_report should return success" do
      url = "http://localhost:4002/api/report"
      file_name = "products_report_test.csv"
      user_email = "marcos.nogueira@skyhub.com.br"

      data = [
        {"to", user_email},
        {"subject", "Relatorio de Produtos"},
        {
          "attachment",
          "00001;Primeiro produto;321.0;12;8902899990\n00002;Produto Dois;123.0;34;890342134\n",
          {"form-data", [{"name", "\"attachment\""}, {"filename", file_name}]},
          [{"Content-Type", "application/json"}]
        }
      ]

      hackney_params = [
        :post,
        url,
        [],
        {:multipart, data},
        []
      ]

      expect(:hackney, :request, [{hackney_params, {:ok, 200, [], :client}}])
      expect(:hackney, :body, 2, {:ok, ""})

      require IEx; IEx.pry()
      assert ReportCsv.send_report("test/fixture/products_report_test.csv", file_name, user_email) ==
               {:ok, "Email Enfileirado para envio"}
    end
  end
end